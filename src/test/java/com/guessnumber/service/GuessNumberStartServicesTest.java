package test.java.com.guessnumber.service;

import main.com.guessnumber.service.GuessNumberStartServices;
import main.com.guessnumber.utils.RandomNum;
import main.com.guessnumber.utils.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberStartServicesTest {


    private final Validation validation = Mockito.mock(Validation.class);
    private final RandomNum randomNum = Mockito.mock(RandomNum.class);

    GuessNumberStartServices cut = new GuessNumberStartServices(randomNum, validation);

    static Arguments[] startGameTestArgs() {
        return new Arguments[]{
                Arguments.arguments(10, 20, 5, 15, 11, 14, 19, 15,
                        "����������! �� ������ ���������� ����� �� 4 �������"),
                Arguments.arguments(50, 200, 10, 51, 51,9,9, 9,
                        "����������! �� ������ ���������� ����� �� 1 �������"),
                Arguments.arguments(40, 89, 3, 66, 52, 80, 70, 65,
                        "�� ��������! �� ��� \"��������\" ����� �� ���� :)")

        };
    }

    @ParameterizedTest
    @MethodSource("startGameTestArgs")
    void startGameTest(int minNumRange, int maxNumRange,
                       int numOfTry, int hiddenNumber,
                       int userNum1, int userNum2, int userNum3,
                       int userNum4, String expected) {
        Mockito.when(validation.getRageNum()).thenReturn(minNumRange, maxNumRange);
        Mockito.when(validation.getTry()).thenReturn(numOfTry);
        Mockito.when(randomNum.getRandomNum(minNumRange, maxNumRange)).thenReturn(hiddenNumber);
        Mockito.when(validation.getValidNum(minNumRange, maxNumRange)).thenReturn(userNum1, userNum2, userNum3, userNum4);

        String actual = cut.startGame();
        Assertions.assertEquals(expected, actual);

    }
}