package test.java.com.guessnumber.utils;

import main.com.guessnumber.utils.RandomNum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;


public class RandomNumTest {

    private final Random random = Mockito.mock(Random.class);
    private final RandomNum cut = new RandomNum(random);

    static Arguments[] getRandomNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5, 5, 5, 0),
                Arguments.arguments(200, 100, 201, 100),
        };
    }

    @ParameterizedTest
    @MethodSource("getRandomNumTestArgs")
    void getRandomNumTest(int expected, int minNumRange, int maxNumRange, int randomNum){
        Mockito.when(random.nextInt(maxNumRange-minNumRange+1)).thenReturn(randomNum);

        int actual = cut.getRandomNum(minNumRange, maxNumRange);

        Assertions.assertEquals(expected, actual);
    }

}
