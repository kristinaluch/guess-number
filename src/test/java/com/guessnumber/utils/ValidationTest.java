package test.java.com.guessnumber.utils;

import main.com.guessnumber.utils.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;


public class ValidationTest {

    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final Validation cut = new Validation(scanner);

    static Arguments[] getValidNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(10, 4, 20, 10),
                Arguments.arguments(170, 128, 200, 170),
        };
    }

    @ParameterizedTest
    @MethodSource("getValidNumTestArgs")
    void getValidNumTest(int expected, int minValue, int maxValue, int userNum){
        Mockito.when(scanner.hasNextInt()).thenReturn(true);
        Mockito.when(scanner.nextInt()).thenReturn(userNum);

        int actual = cut.getValidNum(minValue, maxValue);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getRageNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(10, false, "gsdv", true, 10,  10),
                Arguments.arguments(190, false, "8.1", true, 190,  190 ),
        };
    }

    @ParameterizedTest
    @MethodSource("getRageNumTestArgs")
    void getRageNumTest(int expected, boolean has, String text, boolean has2, int userNum){
        Mockito.when(scanner.hasNextInt()).thenReturn(has, has2);
        Mockito.when(scanner.next()).thenReturn(text);
        Mockito.when(scanner.nextInt()).thenReturn(userNum);

        int actual = cut.getRageNum();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getTryTest(){
        int expected = 5;
        Mockito.when(scanner.hasNextInt()).thenReturn(true);
        Mockito.when(scanner.nextInt()).thenReturn(5);

        int actual = cut.getTry();

        Assertions.assertEquals(expected, actual);

        int expected2 = 15;
        Mockito.when(scanner.hasNextInt()).thenReturn(true);
        Mockito.when(scanner.nextInt()).thenReturn(50, 0, 15);

        int actual2 = cut.getTry();

        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(expected2, actual2);
    }



}
