package main.com.guessnumber.service;



import main.com.guessnumber.utils.RandomNum;
import main.com.guessnumber.utils.Validation;


public class GuessNumberStartServices {

    private final RandomNum randomNum;
    private final Validation validation;

    private static final String MSG_START_GAME = "������, � ������� ����� �� min �� max ������ ���������. " +
            "�������� ������� ��� �� ";
    private static final String MSG_START_GAME_END = " �������!"; //x not init
    private static final String MSG_GET_NUMBER = "������� �����: ";
    private static final String MSG_RANGE = "������� �������� ��������� ����� (�� 1 �� 200)";
    private static final String MSG_GET_MIN = "������� ����������� ����� ���������: ";
    private static final String MSG_GET_MAX = "������� ������������ ����� ���������: ";
    private static final String MSG_INCORRECT_RANGE = "�������� ����� �����������: ������������ �������� ������ ���� ������ ������������!";
    private static final String MSG_NUMBER_OF_TRY = "������� ���������� �������: ";
    private static final String WIN = "����������! �� ������ ���������� ����� �� "; // ����������
    private static final String LOSE_ONE_TRY = "�� ������! �������� ";
    private static final String LOSE_HOT = "�� ������, �� ������!!! �������� ";
    private static final String LOSE_COLD = "�� ������, �������� �������� ";
    private static final String END = " �������";
    private static final String YOU_ARE_LOSER = "�� ��������! �� ��� \"��������\" ����� �� ���� :)";


    public GuessNumberStartServices(RandomNum randomNum, Validation validation) {
        this.randomNum = randomNum;
        this.validation = validation;
    }

    public String startGame() {
        // ������ ��������
        System.out.println(MSG_RANGE);
        System.out.println(MSG_GET_MIN);
        int minNumRange = validation.getRageNum();
        System.out.println(MSG_GET_MAX);
        int maxNumRange = validation.getRageNum();

        if (minNumRange < maxNumRange) {
            // ������ ���������� �������
            System.out.println(MSG_NUMBER_OF_TRY);
            int numOfTry = validation.getTry();
            String resultOfGame = playGame(minNumRange, maxNumRange, numOfTry);
            return resultOfGame;
        }
        return MSG_INCORRECT_RANGE;
    }


    private String playGame(int minNumRange, int maxNumRange, int numOfTry) {

        int hiddenNumber = randomNum.getRandomNum(minNumRange, maxNumRange);

        System.out.println(MSG_START_GAME + numOfTry + MSG_START_GAME_END);
        int tryingToGues = validation.getValidNum(minNumRange, maxNumRange);

        String answer;
        if (hiddenNumber == tryingToGues) {
            // ����� ����� ������ ���������. ���� ���, �������� ��� � ������
            answer = WIN + 1 + END;
            return answer; //(1)
        } else {
        answer = LOSE_ONE_TRY + (numOfTry - 1) + END;
        System.out.println(answer);
        }

        int differenceLast = Math.abs(hiddenNumber - tryingToGues);
        int differenceNow;
        int attemptsLeft;

         //��� ����������� ����. ��� ���������� ������������. �� ������� ����, � ���������, �� �������. ��������(((

        for (int attempt = 2; attempt <= numOfTry; attempt++) {
            tryingToGues = validation.getValidNum(minNumRange, maxNumRange);
            attemptsLeft = numOfTry - attempt;
            if (tryingToGues == hiddenNumber) {
                answer = WIN + attempt + END;
                return answer;
            } else {
                differenceNow = Math.abs(hiddenNumber - tryingToGues);
                answer = getPosition(differenceLast, differenceNow) + attemptsLeft + END;
                System.out.println(answer);

                differenceLast = differenceNow;
            }
        }
        // ���� ������ ���� ���-�� ��� ������, �� ���� ��������� � ���� ����� �����. �� ����������
        System.out.println(YOU_ARE_LOSER);
        return YOU_ARE_LOSER;

    }

    private String getPosition(int difference1, int difference2){

        if (difference2 < difference1) {
            return LOSE_HOT;

        } else {
            return LOSE_COLD;
        }
    }


}

