package main.com.guessnumber;


import main.com.guessnumber.service.GuessNumberStartServices;
import main.com.guessnumber.utils.RandomNum;
import main.com.guessnumber.utils.Validation;

import java.util.Random;
import java.util.Scanner;


public class GuessNumberRun {
    public static void main(String[] args) {

        Random random = new Random(200);
        RandomNum randomNum = new RandomNum(random);
        Scanner scanner = new Scanner(System.in);
        Validation validation = new Validation(scanner);
        GuessNumberStartServices startGame = new GuessNumberStartServices(randomNum, validation);
        while (true) {
            System.out.println(startGame.startGame());
        }
    }
}