package main.com.guessnumber.utils;


import java.util.Scanner;

public class Validation {


    private Scanner scanner;
    public static final String MSG_INCORRECT_NUMBER = "��������� ����� �� �������� ����������� ���������!";
    public static final String MSG_INCORRECT_TEXT = "������� �����, ����� ���������� ��� exit ����� ���������";
    public static final String MSG_EXIT = "�� ��������� ����";
    public static final int minValueRange = 1;
    public static final int maxValueRange = 200;
    public static final int minValueTry = 1;
    public static final int maxValueTry = 15;

    public Validation(Scanner scanner) {
        this.scanner = scanner;
    }


    public int getRageNum(){
        return getValidNum(minValueRange, maxValueRange);
    }

    public int getTry(){
        return getValidNum(minValueTry, maxValueTry);

    }

    public int getValidNum(int minValue, int maxValue) {
        int userNum;
        String userText;
        for (; ; ) {
            if (scanner.hasNextInt()) {
                userNum = scanner.nextInt();
                if (userNum >= minValue && userNum <= maxValue) {
                    return userNum;
                } else {
                    System.out.println(MSG_INCORRECT_NUMBER);
                }
            } else {
                userText = scanner.nextLine();
                checkHaveExit(userText);
            }
        }
    }

    private void checkHaveExit(String userText){
        if (userText.equalsIgnoreCase("exit")) {
            exit();
        } else {
            System.out.println(MSG_INCORRECT_TEXT);
        }
    }

    private void exit() {
        System.out.println(MSG_EXIT);
        System.exit(0);
    }

}