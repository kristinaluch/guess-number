package main.com.guessnumber.utils;

import java.util.Random;

public class RandomNum {
    private Random random;
    private int randomNum;
    public RandomNum(Random random) {
        this.random = random;
    }

    public int getRandomNum(int minNumRange, int maxNumRange){
        randomNum = minNumRange+random.nextInt(maxNumRange-minNumRange+1);
        return randomNum;
    }
}